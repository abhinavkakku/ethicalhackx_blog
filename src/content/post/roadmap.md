---
title: Cyber Security RoadMap (2024)
publishDate: 2024-05-12
updateDate: 2024-05-12
author: Abhinav Kumar
image:   '~/assets/images/roadmap.jpg'
excerpt: Hwo to Get started in Cyber Security to become Hacker, Penetration Tester, SOC Analyst, Security Engineer in 2024.
draft: false
category: Hacking-101
tags:
    - SOC Analyst
    - Security Engineer
    - DFIR
    - Penetration Tester
    - roadmap
---

How to get started in Cyber Security, what are the things one should learn when starting Cyber Security Journey .
There is no definitive answer ! But I have tried to cover few of these things that apply to almost all job domains in Cyber Security.

Cyber Security is in iteself a big world with diferent job profiles, but as a beginner what all to learn, we see that further in this post.

If you already know the things mentioned , just skip it.
The aim is to move fast, later when you have some knowledge of things, you can decide what to spend time on, what topics to study in details and depth.

We are working on simplifying this roadmap and will update it in coming days.

Download the PDF for reference.

<img src="/src/assets/images/Cyber_Security_Roadmap_(Beginners).svg" alt="Description" />
